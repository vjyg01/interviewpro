package readcsvfile;

public class CSVModel {
private String	card_type;
private String pen_auto;
private String department ;
private String card_type_name;
private String 	card_type_short;
private String color_title_bar;
public String getCard_type() {
	return card_type;
}
public void setCard_type(String card_type) {
	this.card_type = card_type;
}
public String getPen_auto() {
	return pen_auto;
}
public void setPen_auto(String pen_auto) {
	this.pen_auto = pen_auto;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public String getCard_type_name() {
	return card_type_name;
}
public void setCard_type_name(String card_type_name) {
	this.card_type_name = card_type_name;
}
public String getCard_type_short() {
	return card_type_short;
}
public void setCard_type_short(String card_type_short) {
	this.card_type_short = card_type_short;
}
public String getColor_title_bar() {
	return color_title_bar;
}
public void setColor_title_bar(String color_title_bar) {
	this.color_title_bar = color_title_bar;
}
@Override
public String toString() {
	return "CSVModel [card_type=" + card_type + ", pen_auto=" + pen_auto + ", department=" + department
			+ ", card_type_name=" + card_type_name + ", card_type_short=" + card_type_short + ", color_title_bar="
			+ color_title_bar + "]";
}
public CSVModel(String card_type, String pen_auto, String department, String card_type_name, String card_type_short,
		String color_title_bar) {
	super();
	this.card_type = card_type;
	this.pen_auto = pen_auto;
	this.department = department;
	this.card_type_name = card_type_name;
	this.card_type_short = card_type_short;
	this.color_title_bar = color_title_bar;
}





}
