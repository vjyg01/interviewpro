package readcsvfile;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.sampled.Line;

public class ReadCSVfileData {
	
	public static void main(String[] args) {
		String url="D:\\card_type_master.csv";
		//Path path=Path.of("Downloads\\data-1709979047921.csv", "");
		//card_type="S", pen_auto="0", department="0", card_type_name="Serving", card_type_short=NULL, color_title_bar="S"]
        List<String> cardTypeList = new ArrayList<>();
        List<String> penAuto = new ArrayList<>();
        List<String> department = new ArrayList<>();
        List<String> cardTypeName = new ArrayList<>();
        List<String> cardTypeShort = new ArrayList<>();
        List<String> colorTitlebar = new ArrayList<>();
        List<String> headerList = new ArrayList<>();
        List<String> expectedHeaders = Arrays.asList("card_type", "pen_auto", "department", "card_type_name", "card_type_short", "color_title_bar");

		Path path = Paths.get(url);
		if(Files.exists(path)) {
			try {
//				Files.lines(path).skip(1).map(line ->{
//					 String[] csvData=line.split(",");
//					 return new CSVModel(csvData[0], csvData[1], csvData[2], csvData[3], csvData[4], csvData[5]);
//				}).forEach(System.out::println);
				 // Read all lines from the file
                List<String> lines = Files.readAllLines(path);

                if (!lines.isEmpty()) {
                    // Read the header line
                    String headerLine = lines.get(0);
                    String[] headers = headerLine.split(",");
                    for (String header : headers) {
                        headerList.add(header.trim());
                    }
				Files.lines(path).skip(1)
				.map(line->line.split(","))
				.forEach(csvData->{
					cardTypeList.add(csvData[0]);
					penAuto.add(csvData[1]);
					department.add(csvData[2]);
					cardTypeName.add(csvData[3]);
					cardTypeShort.add(csvData[4]);
					colorTitlebar.add(csvData[5]);
				});
                    }
				System.out.println("Card Type List: " + cardTypeList);
                System.out.println("Bendi List: " + penAuto);
                System.out.println("department List: " + department);
                System.out.println("cardTypeName List: " + cardTypeName);
                System.out.println("cardTypeShort List: " + cardTypeShort);
                System.out.println("cardTypeShort List: " + cardTypeShort);
                System.out.println("headerList List: " + headerList);
				
                
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			System.out.println("file path is not exists");
		}
	}
}
