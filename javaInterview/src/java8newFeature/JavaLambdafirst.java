package java8newFeature;

import java.util.function.Function;
import java.util.function.Predicate;

import java8newFeature.functionalinterface.functionalInterface;

public class JavaLambdafirst implements functionalInterface{
	public static void main(String[] args) {
		Function<Integer, Integer>f=i->i*i;
		System.out.println("Square of number is " + f.apply(6));
		
		//check number is even or not
		
		Predicate<Integer>f1=i->i%2==0;
		System.out.println("Number is even " + f1.test(8));
		
	}

	@Override
	public void method1() {
		// TODO Auto-generated method stub
		
	}
}

 