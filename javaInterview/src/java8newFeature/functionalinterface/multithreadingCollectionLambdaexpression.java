package java8newFeature.functionalinterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
/*class MyComprator implements Comparator<Integer>{

	@Override
	public int compare(Integer l1, Integer l2) {
		// TODO Auto-generated method stub
		return (l1<l2)?-1:(l1>l2)?1:0;
		
		
		
		 * if(l1<l2) { return -1; } else if(l1>l2) { return +1; } else { return 0; }
		 
	}
	
}*/

public class multithreadingCollectionLambdaexpression {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(20);
		list.add(30);
		list.add(2);
		list.add(35);
		list.add(12);
		list.add(0);
		list.add(5);
		list.add(25);
		System.out.println(list);
		Comparator<Integer> c=(l1,l2)->(l1<l2)?-1:(l1>l2)?1:0;
		Collections.sort(list,c);
		System.out.println(list);
		list.stream().forEach(System.out::println);
		List<Integer> l3=list.stream().filter(i->i%2==0).collect(Collectors.toList());
		System.out.println(l3);
		//Collections.sort(list, new MyComprator());
		//System.out.println(list);
		// System.out.println(list.sort());
		// Sum arraylist all element without using loop only using stream api in java 8
		// feature
		System.out.println(list.stream().mapToInt(i -> i).sum());

	}

}
