package java8newFeature.functionalinterface;

interface Intref{
	public void m1();
	
}
interface Aint{
	public void add(int a,int b);
}
interface Sinterf{
	public int squareIt(int n);
}
/* */


public class LambdaExpressionFunctionalInterFace {
		public static void main(String[] args) {
			/*
			 * Intref inf=new Demo(); inf.m1();
			 */
			
		//using Lambda Expression for functional interfaces
			Intref i=()->System.out.println("Hello...By lambda Expression");
			i.m1();
//Using Lambda expression inside functional interface			
			Aint addinf=(a,b)->System.out.println(a+b);
			addinf.add(20, 20);
			addinf.add(200, -100);
//Using return type in lambda expression in side functional interface 
			
			Sinterf sintf=n->n*n;
			System.out.println(sintf.squareIt(5));
		}
}
