package java8newFeature.functionalinterface;
@FunctionalInterface
public interface functionalInterface {
	
	public void method1();
	public default void method2() {
		System.out.println("hello default");
	}
	public static void method3() {
		System.out.println("Hello static ");
	}
}
