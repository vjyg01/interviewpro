package java8newFeature.functionalinterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Employee{
	String name;
	int eno;
	public Employee(String name, int eno) {
		super();
		this.name = name;
		this.eno = eno;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", eno=" + eno + "]";
	}
	
	
}


public class AnonymousinnerclassVsLambdaexpression {
			public static void main(String[] args) {
				Employee e=new Employee("Ram", 100);
				System.out.println(e);
				
			ArrayList<Employee> l=new ArrayList<>();
			l.add(new Employee("vijay",1));
			l.add(new Employee("sunny",2));
			l.add(new Employee("raghu",9));
			l.add(new Employee("bhola",11));
			l.add(new Employee("ram",5));
			l.add(new Employee("shyam",17));
			System.out.println(l);
			Collections.sort(l,(e1,e2)->(e1.eno<e2.eno)?-1:(e1.eno>e2.eno)?1:0);
			System.out.println(l);
			
			Collections.sort(l,(e1,e2)->e1.name.compareTo(e2.name));
			System.out.println(l);
			
			//sort based on alphabetical order of emp names
			
			

			}
}
