package collection;

import java.util.Iterator;
import java.util.PriorityQueue;

public class PrioritesQueue {
	public static void main(String[] args) {
		PriorityQueue<String> queue=new PriorityQueue<>();
		queue.add("Ajay");
		queue.add("vijay");
		queue.add("ram");
		queue.add("shyam");
		queue.add("puja");
		System.out.println("head : " + queue.element());
		System.out.println("head : "+queue.peek());
		System.out.println("iterating queue element ");
		@SuppressWarnings("rawtypes")
		Iterator itr = queue.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
		queue.remove();
		queue.poll();
		System.out.println("after removing two element"); 
		Iterator<String> itr1=queue.iterator();
		while (itr1.hasNext()) {
			System.out.println(itr1.next());
		}
	}

}
