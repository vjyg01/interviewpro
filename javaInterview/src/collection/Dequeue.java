package collection;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class Dequeue {
	public static void main(String[] args) {
		Deque<String> dq=new ArrayDeque<>();
		dq.add("roshan");
		dq.add("vijay");
		dq.add("puja");
		dq.add("shalini");
		dq.add("ram");
		//traversing element
		for (String ele : dq) {
			System.out.println(ele);
		}
	}

}
