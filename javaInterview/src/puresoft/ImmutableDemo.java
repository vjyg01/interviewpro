package puresoft;

 final class Employee{
	
	final String pancardNumber;

	public Employee(String pancardNumber) {
		super();
		this.pancardNumber = pancardNumber;
	}
	
	public String getpancardNumber() {
		return pancardNumber;
		
	}
	
	 
}


public class ImmutableDemo {
	public static void main(String[] args) {
		Employee e=new Employee("abcde");
		String s1=e.getpancardNumber();
		System.out.println("Pan Card Number " + s1);
	}

}
