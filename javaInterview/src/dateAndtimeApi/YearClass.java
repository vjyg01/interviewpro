package dateAndtimeApi;

import java.time.Year;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Scanner;

public class YearClass {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter the year");
	int n=sc.nextInt();
	Year y=Year.of(n);
	if(y.isLeap()) {
		System.out.printf("%d year is leap year ",n);
	}else {
		System.out.printf("%d Year is not Leap Year",n);
	}
	
	System.out.println("\n\n");
	
	ZoneId zone=ZoneId.systemDefault();
	System.out.println(zone);
	
	ZoneId la=ZoneId.of("America/Los_Angeles");
	ZonedDateTime dt=ZonedDateTime.now(la);
	System.out.println(dt);
}
}
