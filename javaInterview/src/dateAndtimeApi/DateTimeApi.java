package dateAndtimeApi;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;

public class DateTimeApi {
		public static void main(String[] args) {
			LocalDate date=LocalDate.now();
			System.out.println(date);
			LocalTime time=LocalTime.now();
			System.out.println(time);
		
		int dd=date.getDayOfMonth();
		int mm=date.getMonthValue();
		int yyyy=date.getYear();
		
		System.out.println(dd + "..." + mm + "..." + yyyy);
		System.out.printf("%d-%d-%d",dd,mm,yyyy);
		System.out.println("\n\n");
		
		System.out.println("print time min,second,nano-second");
		
		int h=time.getHour();
		int m=time.getMinute();
		int s=time.getSecond();
		int n=time.getNano();
		System.out.println(h + " : " + m + " : " + s+ " : " + n);
		System.out.printf("%d:%d:%d:%d",h,m,s,n);		
		
		System.out.println("\n\n");
		LocalDateTime dt=LocalDateTime.now();
		System.out.println(dt);
		
		int dd1=dt.getDayOfMonth();
		int mm1=dt.getMonthValue();
		int yy1=dt.getYear();
		System.out.printf("%d-%d-%d",dd1,mm1,yy1);
		int h1=dt.getHour();
		int m1=dt.getMinute();
		int s1=dt.getSecond();
		int n1=dt.getNano();
		System.out.printf("\n%d:%d:%d:%d",h1,m1,s1,n1);
		
		System.out.println("\n");
		LocalDateTime dt1=LocalDateTime.of(1995,Month.MAY,28,12,45);
		System.out.println(dt1);
		System.out.println("After Six Months :" + dt1.plusMonths(6));
		System.out.println("Before six Months :" + dt1.minusMonths(6));
		
		System.out.println("\n");
		LocalDate birthday=LocalDate.of(1991,8,28);
		LocalDate today=LocalDate.now();
		Period p=Period.between(birthday, today);
		System.out.printf("year age is %d years % dmonth %d days",
				p.getYears(),p.getMonths(),p.getDays());
		
		
		LocalDate deathDay=LocalDate.of(1989+60, 8, 28);
		Period p1=Period.between(today, deathDay);
		int d=p1.getYears()*365+p1.getMonths()*30+p1.getDays();
		System.out.printf("\n you will be on each only %d days,"
				+ "Hueey up to do important things ",d);
		
		}
		
}
