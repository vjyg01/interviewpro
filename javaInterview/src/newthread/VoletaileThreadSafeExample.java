package newthread;

public class VoletaileThreadSafeExample {
	private final static int noOfThreads=2;
	public static void main(String[] args) {
		VolatieData volatieData=new VolatieData();
		Thread[] threads=new Thread[noOfThreads];
		
		for (int i = 0; i < noOfThreads; i++) {
			threads[i]=new VolatileThread(volatieData);
		}
		for (int i = 0; i < noOfThreads; i++) {
			threads[i].start();
		}
		for (int i = 0; i < noOfThreads; i++) {
			try {
				threads[1].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}



class VolatieData{
	
	private volatile int counter=0;
	public int getCounter() {
		return counter;
		
	}
	
	public void increaseCounter() {
		++counter;
	}
}

class VolatileThread extends Thread {
	private final VolatieData data;
	public VolatileThread(VolatieData data) {
		this.data=data;
	}
	@Override
	public void run() {
		int oldValue=data.getCounter();
		System.out.println("Thread " + Thread.currentThread().getId() + " old value " + oldValue);
		data.increaseCounter();
		int newValue=data.getCounter();
		System.out.println("thread " + Thread.currentThread().getId() + " new Value  " + newValue);
	}
	
} 
