import java.io.Serializable;

class TransientExample implements Serializable {

	transient int age;
	private String name;
	private String address;
	
}




public class TansientandVolatile  extends Thread{

	 private boolean isRunning =true;

	public void run() {
		long count=0;
		while (isRunning) {
		count++;
		
		}
		System.out.println("Thread terminated " + count);
	}
	
	public static void main(String[] args) {
		TansientandVolatile t = new TansientandVolatile();
	      t.start();
	      try {
			Thread.sleep(2000);
			 t.isRunning=false;
		      t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
//	      Thread.sleep(2000);
//	      
//	      t.isRunning  = false;
//	      t.join();
	      System.out.println("isRunning set to " + t.isRunning);
		
	}
	
	
}
