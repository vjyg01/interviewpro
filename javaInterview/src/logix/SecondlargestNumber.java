package logix;

public class SecondlargestNumber {
   
	public static void main(String[] args) {
		int num[]= {1,4,7,9,23,7,1};
		int n=num.length;
		int highestNum=Integer.MIN_VALUE;
		int SecondHighestNum=Integer.MIN_VALUE;
		
		for (int i = 0; i < n; i++) {
			if(num[i]>highestNum) {
				SecondHighestNum=highestNum;
				highestNum=num[i];
			}
			if(num[i]<highestNum && num[i]>SecondHighestNum) {
				SecondHighestNum=num[i];
			}
			
		}
		System.out.println("second Highest Number in arrayList " + SecondHighestNum);
	}
}
