package logix;

import java.util.HashMap;
import java.util.Map;

public class JSONParserExample {
    public static void main(String[] args) {
        // The given sample JSON string
        String json = "{\"name\":\"John\",\"age\":30,\"city\":\"New York\"}";

        // Custom parser to convert JSON string to Java object
        Map<String, Object> jsonObj = parseJSON(json);

        // Print the values for the keys asked in the queries
        System.out.println("Name: " + jsonObj.get("name"));
        System.out.println("Age: " + jsonObj.get("age"));
        System.out.println("City: " + jsonObj.get("city"));
    }

    // Custom parser to convert JSON string to Java object
    private static Map<String, Object> parseJSON(String json) {
        Map<String, Object> jsonObj = new HashMap<String, Object>();

        // Since we know the structure of the JSON string, we can split it by ',' and ':'
        // to extract each key-value pair and put them in the map
        String[] kvPairs = json.replaceAll("\\{|\\}", "").split(",");
        for (String kvPair : kvPairs) {
            String[] parts = kvPair.split(":");
            jsonObj.put(parts[0].replaceAll("\"", "").trim(), parts[1].trim());
        }

        return jsonObj;
    }
}
