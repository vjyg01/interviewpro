package streamApi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

public class Main {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Alice", 25));
        people.add(new Person("Bob", 30));
        people.add(new Person("Charlie", 20));
        people.add(new Person("Charlie2", 80));
        people.add(new Person("Charlie3", 31));
        people.add(new Person("Charlie4", 19));
        people.add(new Person("Charlie6", 25));

        // Filter people who are older than 25
        List<Person> filteredPeople = people.stream()
                .filter(person -> person.getAge() > 25).collect(Collectors.toList());

        // Print the filtered people
        filteredPeople.forEach(person -> System.out.println(person.getName()+ "-"+ person.getAge()));
    }
}