package streamApi;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CheckEvenNumber {
		/**
		 * @param args
		 */
		/**
		 * @param args
		 */
		public static void main(String[] args) {
			ArrayList<Integer>l=new ArrayList<>();
			l.add(0);
			l.add(5);
			l.add(10);
			l.add(20);
			l.add(30);
			l.add(25);
			System.out.println(l);
			List<Integer> list=l.stream().filter(i->i%2==0).collect(Collectors.toList());
			System.out.println(list);
			System.out.println("***************************************");
			System.out.println("\n");
			ArrayList<Integer>marks=new ArrayList<>();
			marks.add(70);
			marks.add(45);
			marks.add(10);
			marks.add(60);
			marks.add(30);
			marks.add(25);
			System.out.println(marks);
			List<Integer>updateMarks=marks.stream().map(i->i+5).collect(Collectors.toList());
			System.out.println(updateMarks);
			int sum=marks.stream().mapToInt(i->i).sum();
			int sum1=updateMarks.stream().mapToInt(i->i).sum();
			System.out.println(sum + " : " + sum1 + " : " + sum);
			long noOfFailedstudent=marks.stream().filter(i->i<35).count();
			System.out.println(noOfFailedstudent);
			System.out.println("\n");
			System.out.println("*****sorted order natural order********");
			List<Integer> sortelements=marks.stream().sorted().collect(Collectors.toList());
			System.out.println(sortelements);
			
			System.out.println("\n");
			System.out.println("*****sorted order custom order********");
			List<Integer> naturalsort=marks.stream().sorted((i1,i2)->-i1.compareTo(i2)).collect(Collectors.toList());
			System.out.println(naturalsort);
			//sorted order
			System.out.println("\n");
			System.out.println("*****sorted order customized desending order********");
			List<Integer>sort=marks.stream().sorted((i1,i2)->((i1<i2)?1:(i1>i2)?-1:0)).collect(Collectors.toList());
				System.out.println(sort);	
			//Min and Max value in array list
			Integer min=	marks.stream().min((i1,i2)->i1.compareTo(i2)).get();
				System.out.println("Min value of natural sorting : " + min);
				
				Integer max=	marks.stream().max((i1,i2)->i1.compareTo(i2)).get();
				System.out.println("max value of natural sorting : " + max);
				
				
				ArrayList<String> sl=new ArrayList<>();
				sl.add("sunny");
				sl.add("kajal");
				sl.add("Prabhas");
				sl.add("Anushka");
				sl.add("Mailka");
				System.out.println(sl);
		
 //Natural sorting order  ==>  (s1,s2)->s1.compareTo(s2)
//Reverse sorting order   ==>  (s1,s2)->s2.compareTo(s1)				
//Reverse of natural sorting order  ==> (s1,s2)->-s1.compareTo(s2)				
				
			System.out.println("*******Natural Sorting**********");	
				List<String> soList=sl.stream().sorted().collect(Collectors.toList());
				System.out.println(soList);
				
				System.out.println("\n");
				System.out.println("*******custom Sorting**********");
				List<String> soList2=sl.stream().sorted((s1,s2)->s1.compareTo(s2)).collect(Collectors.toList());
				System.out.println(soList2);
				
				
				System.out.println("\n");
				System.out.println("*******Reverse Natural Sorting**********");		
				List<String> soList1=sl.stream().sorted((s1,s2)->s2.compareTo(s1)).collect(Collectors.toList());
				System.out.println(soList1);
				
				System.out.println("\n");
				System.out.println("*******Sorting according to length **********");		
				Comparator<String> c=(s1,s2)->{
					int l1=s1.length();
					int l2=s2.length();
					if(l1<l2)return-1;
					else if(l1>l2)return 1;
					else return s1.compareTo(s2);
					
				};
				
				List<String>li=sl.stream().sorted(c).collect(Collectors.toList());
				System.out.println(li);
				
				ArrayList<Integer>list1=new ArrayList<>();
				list1.add(10);
				list1.add(11);
				list1.add(12);
				list1.add(29);
				list1.add(15);
				list1.add(5);
				System.out.println("\n");
				System.out.println("*****print list one by one*****");
				System.out.println(list1);
				/*
				 * Consumer<Integer>c1=i->{ System.out.println("The Square of " + i + " is: " +
				 * (i*i)); };
				 */
			list1.stream().forEach(i->{
				System.out.println("The Square of " + i + " is: " + (i*i));
			});
			list1.stream().forEach(System.out::println);
			
	//convert arraylist to array object using constructor reffrence
			Integer[] i=list1.stream().toArray(Integer[] :: new );
			/*
			 * for (Integer integer : i) { System.out.println(i); }
			 */
			System.out.println("\n");
			Stream.of(i).forEach(System.out::println);
			
		}
}
