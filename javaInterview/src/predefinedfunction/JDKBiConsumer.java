package predefinedfunction;

import java.util.ArrayList;
import java.util.function.BiConsumer;

class Employee2{
	String name;
	double salary;
	 Employee2(String name, double salary) {
		super();
		this.name = name;
		this.salary = salary;
	}
	
	
}
public class JDKBiConsumer {
	public static void main(String[] args) {
		ArrayList<Employee2>l=new ArrayList<Employee2>();
		popuate(l);
		BiConsumer<Employee2, Double> c=(e,d)->e.salary=e.salary+d;
		for (Employee2 employee2 : l) {
			c.accept(employee2, (double) 500);
		}
		for (Employee2 employee2 : l) {
			System.out.println("Employee name :" + employee2.name + "  Employee salary : " + employee2.salary);
		}
	}

	private static void popuate(ArrayList<Employee2> l) {
		// TODO Auto-generated method stub
		l.add(new Employee2("Durga", 1000));
		l.add(new Employee2("sunny", 2000));
		l.add(new Employee2("Bunny", 3000));
		l.add(new Employee2("chinny", 4000));
		
	}
}
