package predefinedfunction;

import java.util.*;
import java.util.function.*;

 class Employee1 {
	int eno;
	String name;
	

	public Employee1(int eno, String name) {
		super();
		this.eno = eno;
		this.name = name;
		
	}

}

public class JavaBiFunctionmethod {
	public static void main(String[] args) {
		ArrayList<Employee1> l = new ArrayList<Employee1>();
		BiFunction<Integer, String, Employee1> f = (eno, name) -> new Employee1(eno, name);
		l.add(f.apply(100, "Durga"));
		l.add(f.apply(200, "Ravi"));
		l.add(f.apply(300, "sunny"));
		l.add(f.apply(400, "bunny"));
		l.add(f.apply(508, "Barkha"));
		for (Employee1 emp : l) {
			System.out.println("Employee Number :" + emp.eno + " Employee Name :" + emp.name);
		}
		
		
		
	}
	
}
