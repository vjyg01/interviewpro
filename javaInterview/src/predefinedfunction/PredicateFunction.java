package predefinedfunction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.*;
class Employee{
	String name;
	double salary;
	Employee(String name,double salary){
		this.name=name;
		this.salary=salary;
	}
}



//Write a Predicate check whether length is String is > 5 or not.
public class PredicateFunction {
	public static void main(String[] args) {
		String[] s= {"vijay","tribhuwan","ghanshyam","akshay","shalini"};
		Predicate<String>p=s1->s1.length()>5;   //p=s1->s1.length()%2==0
		for (String string : s) {
			if(p.test(string)) {
				System.out.println(string);
			}
		}
		
	ArrayList<Employee>l=new ArrayList<>();
	l.add(new Employee("Durga",1000));
	l.add(new Employee("Ravi",2000));
	l.add(new Employee("Shiva",3000));
	l.add(new Employee("Mahesh",4000));
	l.add(new Employee("Adarsh",5000));
	l.add(new Employee("sagar",6000));
	
	Predicate<Employee>P=e->e.salary>3000;
	for (Employee employee : l) {
		if(P.test(employee)) {
			System.out.println(employee.name + " : " + employee.salary);
		}
	}
	
int[] x= {0,5,10,15,20,25,30,35};
Predicate<Integer>p1=i->i%2==0;
Predicate<Integer>p2=i->i>10;
//and(),or(),negatve()
System.out.println("The number which are even and >10 in array list");
for (int j : x) {
	if(p1.and(p2).test(j)) {
		System.out.println(j);
	}
	
}

System.out.println("The number which are even or >10 in array list");
for (int j : x) {
	if(p1.or(p2).test(j)) {
		System.out.println(j);
	}
}
		
		
	}

}
