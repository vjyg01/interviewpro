package predefinedfunction;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;


class Movei{
	 String name;
	 Movei(String name){
		 this.name=name;
	 }
 }

public class JavaConsumer {

	public static void main(String[] args) {
		Consumer<Movei>c1=m->System.out.println(m.name+"Ready to realease");
		Consumer<Movei>c2=m->System.out.println(m.name+"realease but bigger flop");
		Consumer<Movei>c3=m->System.out.println(m.name+"storing info in data base");
	   Consumer<Movei> cc=c1.andThen(c2).andThen(c3);
		
		Movei m=new Movei("Spider");
	    cc.accept(m);
	    
	 //Suppliers
	    Supplier<Date> s=()->new Date();
	    System.out.println(s.get());
	    System.out.println(s.get());
	    System.out.println(s.get());
	    System.out.println(s.get());
	    
	//using Suppliers for print random otp
	    Supplier<String> stp=()->{
	    	String otp="";
	    	for(int i=0;i<6;i++) {
	    		otp=otp+(int)(Math.random()*10);
	    	}
	    	return otp;
	    };
	    
	    System.out.println(stp.get());
	    System.out.println(stp.get());
	    System.out.println(stp.get());
	    System.out.println(stp.get());
	}
}
