package predefinedfunction;

import java.util.Scanner;

interface intraf{
	public static void m1() {
		System.out.println("Interface static method");
	}
}



public class JavaStaticMethod {
	public static void main(String[] args) {
		intraf.m1();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the first Number");
		int a=sc.nextInt();
		System.out.println("enter the second number");
		int b=sc.nextInt();
		System.out.println("enter the third number");
		int c=sc.nextInt();
		int max=(a>b && a>c)?a:(b>c)?b:c;
		System.out.println("max value " + max);
	}
}
