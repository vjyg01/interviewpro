package predefinedfunction;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

public class AdditionalFunctionalinterface {
	public static void main(String[] args) {
		int[] x= {0,5,10,15,20,25,30};
		//Predicate<Integer>p1=j->j+(j+1);
		//Predicate<Integer>p=i->i%2==0;
		// System.out.println(p.test(10));
		IntPredicate p=i->i%2==0;
		for (int j : x) {
			if(p.test(j)) {
				System.out.println(j);
			}
			
			
			
		}
		System.out.println("\n");
		Function<Integer, Integer> f=k->k*k;
		System.out.println(f.apply(4));
		
		System.out.println("\n");
		Function<String, Integer>fstoi=s->s.length();
		System.out.println(fstoi.apply("vijay"));
		
		System.out.println("\n");
		System.out.println("***********ToIntFunction*************");
		
		ToIntFunction<String>fti=st->st.length();
		System.out.println(fti.applyAsInt("vijay"));
		
		System.out.println("\n");
		System.out.println("*******Using Integerand double value**********");
		Function<Integer, Double>intod=m->Math.sqrt(m);
		System.out.println(intod.apply(7));
		
		System.out.println("\n");
		System.out.println("*****intToDoublefunction******");
		IntToDoubleFunction intodo=in->Math.sqrt(in);
		System.out.println(intodo.applyAsDouble(9));
		
		System.out.println("\n");
		System.out.println("*******UnaryOperator**********");
		UnaryOperator<Integer>uo=u->u*u;
		System.out.println(uo.apply(5));
		
		
		System.out.println("\n");
		System.out.println("*******IntUnaryOperator**********");
		IntUnaryOperator uop=un->un*un;
		System.out.println(uop.applyAsInt(5));
		
		
		System.out.println("\n");
		System.out.println("*******BinaryFunction**********");
		BiFunction<String, String, String> bf=(b1,b2)->b1+b2;
		System.out.println(bf.apply("vijay", "Software"));
		
		
		System.out.println("\n");
		System.out.println("*******BinaryOperator**********");
		BinaryOperator<String>b=(bo1,bo2)->bo1+bo2;
		System.out.println(b.apply("vijay", "software"));
		
		
	}

}
