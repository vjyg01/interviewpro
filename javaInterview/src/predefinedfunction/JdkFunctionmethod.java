package predefinedfunction;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
class Student{
	String name;
	int marks;
	public Student(String name,int marks) {
		// TODO Auto-generated constructor stub
	
	this.name=name;
	this.marks=marks;
	}
}
public class JdkFunctionmethod {
	public static void main(String[] args) {
		Function<Integer, Integer> fun1=i->2*i;
		Function<Integer, Integer> fun2=i->i*i*i;
		System.out.println(fun1.andThen(fun2).apply(2));
		System.out.println(fun1.compose(fun2).apply(2));
			
		Function<String, String>f=s->s.toUpperCase();
		Function<String, Integer>f1=i->i.length();
		System.out.println(f.apply("durgasoftsolutions"));
		System.out.println(f1.apply("durgasoftsolutions"));
	Function<Student, String>f2=st->{
		int marks=st.marks;
		String grade="";
		if(marks>=80) grade="A[Dictinction]";
		else if(marks>=60) grade="B[First Class]";
		else if(marks>=50) grade="C[Second Class]";
		else if(marks>=35) grade="D[Third Class]";
		else grade="E[failed]";
		return grade;
	};
	Predicate<Student>p=st->st.marks>=60;
	//consumer
			Consumer<Student> con=st1->{
				System.out.println("Student Name " + st1.name);
				System.out.println("Student  Marks "+ st1.marks);
				System.out.println("Student Grade " + f2.apply(st1));
			};
	Student[] ss= {new Student("Durga",100),
			new Student("sunny",65),
			new Student("bunny",55),
			new Student("chinny",45),
			new Student("vinny",25)};
	
	for (Student student : ss) {
		if(p.test(student)) {
			System.out.println("Student Name:" + student.name);
			System.out.println("Student marks:" + student.marks);
			System.out.println("Student Grade: " + f2.apply(student));
	}
		
		
	
	}
	
	
	  for (Student st1 : ss) 
	  {
		  if(p.test(st1)) {
			  con.accept(st1);
		  }
	 }
}
	
	

}
