package predefinedfunction;
class Student1{
	String name;
	int rollno;
	int marks;
	int age;
	Student1(String name,int rollno,int marks,int age){
		this.name=name;
		this.rollno=rollno;
		this.marks=marks;
		this.age=age;
	}
}



class Sample{
	/*
	 * Sample(){ System.out.println("sample class constructor execution..."); }
	 */
	Sample(String s){
		System.out.println("sample class constructor execution..." + s);
	}
}
interface Interf{
	public void add(int a,int b);
}
interface Interf1{
	//public Sample get();
	public Sample get(String s);
}
interface Interf2{
	//public Sample get();
	public Student1 get(String name,int rollno,int marks,int age);
}

/*
 * class Demo implements Interf2{
 * 
 * @Override public Student1 get(String name, int rollno, int marks, int age) {
 * // TODO Auto-generated method stub Student1 s=new Student1(name, rollno,
 * marks, age); return s; }
 * 
 * }
 */

public class TestMethodRefference {
	public static void sum(int x,int y) {
		System.out.println("The sum :" + (x+y));
	}
	public static void main(String[] args) {
		Interf i=(a,b)->System.out.println("The sum : " + (a+b));
		i.add(10,20);
//method refference		
		Interf i1=TestMethodRefference::sum;
		i1.add(100, 200);
//constructor class refference eg:- Test:: new  (contructor reference)		
		Interf1 inf=Sample::new;
		//Sample s1=inf.get();
		Sample s1=inf.get("vijay");
		
		Interf2 ii=(name,rollno,marks,age)->new Student1(name,rollno,marks,age);
		//nterf2 ii=Student1::new;
		Interf2 ii1=Student1::new;
		Student1 ss=ii1.get("vijay", 11, 100, 30);
	} 
	
}
