package com.volkas;

import java.util.HashMap;
import java.util.Map;

/*Given an array as below:
["India", "Russia", "India", "Greece", "China", "India", "Japan", "China", "Russia"]

Write a piece of code which will print the most repeating country in the given array along with its number of occurences

So the final output will be:*/



public class Test {
	
	public static void main(String[] args) {
		String[]  country= {"India", "Russia", "India", "Greece", "China", "India", "Japan", "China", "Russia"};
	
			HashMap<String, Integer> countrycount=new HashMap<>();
			for (String cntry : country) {
				if(countrycount.containsKey(cntry)) {
					countrycount.put(cntry, countrycount.get(cntry)+1);
				}else {
					countrycount.put(cntry, 1);
				}
				
			}
			
			String repeatedcountry="";
			int maxcount=0;
			for(Map.Entry<String, Integer> entry:countrycount.entrySet()) {
				if(entry.getValue()>maxcount) {
					repeatedcountry=entry.getKey();
					maxcount=entry.getValue();
				}
			}
			
			System.out.println("most repeating country " + repeatedcountry + "  Number of count " + maxcount );
	
	
	}
	

}


//empid

//select * from Employee order by empid Asc|desc;


