package hashMapinternallyWork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MainExample {
	public static void main(String[] args) {
		
		HashMap<String, Integer> friends=new HashMap<>();
		friends.put("ankit", 20);
		friends.put("Uttam", 25);
		friends.put(null, 12);
		System.out.println(friends);
		
//Conversion Array into ArrayList
		// Array declaration and initialization
	    String cityNames[]={"Agra", "Mysore", "Chandigarh", "Bhopal"};
	    ArrayList<String> arl=new ArrayList<String>(Arrays.asList(cityNames));
	    System.out.println(arl);
	    for (String str: arl)
	    {
	      System.out.println(str);
	    }
	  }
	

}
