package javaInterview;

public class AnonymousClass {
	public static void main(String[] args) {
		
		AnonymousDemo an=new AnonymousDemo();
		an.createClass();
	}
	

}

class Polygon{
	public void display() {
		System.out.println("Inside the Polygon class ");
	}
}


class AnonymousDemo {
	public void createClass() {
	//	creation of anonymous class and extending class polygon
		Polygon p=new Polygon() {
			public void display() {
				System.out.println(" Inside an anonymous class ");
			}
		};
		p.display();
	}
	
}