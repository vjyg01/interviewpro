package javaInterview;

import java.util.ArrayList;


class A {
	  public A() {
	    System.out.println("A's no-arg constructor is invoked");
	  }
	}

	class B extends A {
	}
public class InterviewQuestionAns {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.remove(1);
		//list.remove(1);
		System.out.println(list);
		  B b = new B();
	}

}
