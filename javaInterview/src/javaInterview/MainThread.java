package javaInterview;

public class MainThread {
	public static void main(String[] args) {
		Thread t=Thread.currentThread();
		System.out.println("Main Thread : " + t);
		t.setName("current");
		System.out.println("current Thread : " + t);
		try {
			for (int i = 1; i <=5; i++) {
				
				System.out.println(i);
				Thread.sleep(10);
			}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Main thread is intrupted");
		}
		System.out.println("Exit the Main Thread");
	}

}
