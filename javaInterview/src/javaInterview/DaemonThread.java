package javaInterview;

public class DaemonThread extends Thread {
	
	@Override
	public void run() {
			
		System.out.println("Name :" + Thread.currentThread().getName());
		System.out.println("DaemonThread : " + Thread.currentThread().isDaemon());
		
	}
	public static void main(String[] args) {
		DaemonThread t1=new DaemonThread();
		DaemonThread t2=new DaemonThread();
		t1.start();
		t1.setDaemon(false);
		t2.start();
	}

}
