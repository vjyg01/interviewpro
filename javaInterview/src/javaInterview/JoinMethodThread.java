package javaInterview;

public class JoinMethodThread extends Thread{
	@Override
	public void run() {
		for (int i = 1; i <=5; i++) {
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				System.out.println(e);
				
			}
			System.out.println(i);
		}	
		
	}
	public static void main(String[] args) {
		JoinMethodThread t1=new JoinMethodThread();
		JoinMethodThread t2=new JoinMethodThread();
		JoinMethodThread t3=new JoinMethodThread();
		t1.start();
		try {
			t1.join();
		
		} catch (Exception e) {
			System.out.println(e);
		}
		t2.start();
		t3.start();
	}

}
