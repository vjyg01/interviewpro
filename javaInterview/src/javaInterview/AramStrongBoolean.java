package javaInterview;

import java.util.Scanner;

public class AramStrongBoolean {
	public static void main(String[] args) {
		int num;
		Scanner sc =new Scanner(System.in);
		num=sc.nextInt();
		if(isArmstrong(num)) {
			System.out.println(num + " is an ArmStrong Number ");
		}else {
			System.out.println(num + " is not an ArmStrong Number");
		}
	}

	private static boolean isArmstrong(int num) {
		// TODO Auto-generated method stub
		int sum=0,digit,temp;
		temp=num;
		while(num>0) {
			digit=num%10;
			sum=sum+(digit*digit*digit);
			num=num/10;
		}
		return temp==sum;
	}
}
