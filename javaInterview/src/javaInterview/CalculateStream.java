package javaInterview;

import java.util.Arrays;
import java.util.Scanner;

public class CalculateStream {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String st=sc.nextLine();
		int[] freq=new int[st.length()];
		int i,j;
		char String[]=st.toCharArray();
		for(i=0;i<st.length();i++) {
			freq[i]=1;
			for(j=i+1;j<st.length();j++) {
				if(String[i]==String[j]) {
					freq[i]++;
					String[j]='0';
				}
			}
		}
		
		System.out.println("Characters and their corresponding string");
		for(i=0;i<freq.length;i++) {
			if(String[i]!=' ' && String[i]!= '0') {
				System.out.println(String[i]+ "-"+ freq[i]);
			}
		}
		
		//  int[] A = { 1, 2, 3, 4, 5 };
	      //  int sum = Arrays.stream(A).sum();
		 // int sum = Arrays.stream(A).reduce(Integer::sum).getAsInt();
	 
	       // System.out.println("The sum of all the array elements is " + sum);
		
	}
	
	
	
	

}
