package javaInterview.imutableclassAccessmutableobject;

public class Address implements Cloneable {
	 String city;
	 String lineno;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLineno() {
		return lineno;
	}
	public void setLineno(String lineno) {
		this.lineno = lineno;
	}
	public Address(String city, String lineno) {
		super();
		this.city = city;
		this.lineno = lineno;
	}
	@Override
	public String toString() {
		return "Address [city=" + city + ", lineno=" + lineno + "]";
	}
	
	 
	

}
