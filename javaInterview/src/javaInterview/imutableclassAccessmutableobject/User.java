package javaInterview.imutableclassAccessmutableobject;

public final class User {
	private final String name;
	private final String lname;
	private final Address address ;
	public String getName() {
		return name;
	}
	public String getLname() {
		return lname;
	}
	
	
	public Address getAddress() {
		return address;
	}
	public User(String name, String lname, Address address) {
		super();
		this.name = name;
		this.lname = lname;
		this.address = address;
	}
	
	
	
}
