package javaInterview;

class ABC implements Runnable{
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(100);
		} catch (InterruptedException ie) {
			// TODO: handle exception
			ie.printStackTrace();
		}
		
		System.out.println("The state of t1 while it invoked the join() on thread t2 " + DemoStratingThread.t1.getState());
	
	try {
		Thread.sleep(200);
	} catch (InterruptedException e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	}
}


public class DemoStratingThread implements Runnable {
public static Thread t1;
public static DemoStratingThread obj;
public static void main(String[] args) {
	obj=new DemoStratingThread();
	t1=new Thread(obj);
	System.out.println("The state of thread t1 after spawning it - " + t1.getState());
	t1.start();
	System.out.println("The state of t1 after invoking the method start() on it -" + t1.getState());
}
public void run() {
	ABC myObj=new ABC();
	Thread t2=new Thread(myObj);
	System.out.println("the state of thread t2 after spawning it -" + t2.getState());

try {
	Thread.sleep(200);
} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
}
System.out.println("The state of thread t2 after invoking the method sleep() on it -" + t2.getState());
try {
	t2.join();
}catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
}
System.out.println("The state of thread t2 when it has omplete it's execution -" + t2.getState());
}
	
}
