package javaInterview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Test {
	public static void main(String[] args) {
//		HashMap<String, String> haloCars = new HashMap<String, String>();
//
//		haloCars.put("ford", "GT");
//		haloCars.put("dodge", "Viper");
//		//next line wipes out the value "GT" in the "ford" key
//		haloCars.put("ford", "Mustang Mach-E");
//
//		System.out.println(haloCars.get("ford"));
		
		Map<String, ArrayList<String>> multiValueMap = new HashMap<String, ArrayList<String>>();

		multiValueMap.put("ford", new ArrayList<String>());
		multiValueMap.get("ford").add("GT");
		multiValueMap.get("ford").add("Mustang Mach-E");
		multiValueMap.get("ford").add("Pantera");
		System.out.println(multiValueMap);
	}

}
