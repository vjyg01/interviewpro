package preventbreaksingletonDpatteren;

 final class Singleton{
	 private static volatile Singleton instance=null;
	private Singleton() {
		
	}
	public static Singleton getInstance() {
		if(instance==null) {
			synchronized (Singleton.class) {
				if(instance==null) {
					instance=new Singleton();
				}
			}
		}
		return instance;
	}
	
	
	
}

public class SingletonTest {
	
	public static void main(String[] args) {
		Singleton obj1=Singleton.getInstance();
		Singleton obj2=Singleton.getInstance();
		System.out.println("Hash of Object1 " + obj1.hashCode());
		System.out.println("Hash of Obj2 " + obj2.hashCode());
	}
	
}

//both object get same hash code show to break that pattern used three method 
//1.Reflection   2.Deselization   3.cloning for break same singleton design pattern 



