package preventbreaksingletonDpatteren;


class SuperClass implements Cloneable {
	 
    int i = 10;
 
    @Override
    protected Object clone()
        throws CloneNotSupportedException
    {
        return super.clone();
    }
}

class Singleton1 extends SuperClass{
	public static Singleton1  instance=(Singleton1) new Singleton1();
	private Singleton1() {
		
	}
}




public class Clone implements Cloneable  {
	@Override
    protected Object clone() throws CloneNotSupportedException  {
        return super.clone();
    }
	public static void main(String[] args) throws CloneNotSupportedException,Exception {
		// TODO Auto-generated method stub
		
		 Singleton1 instanceOne = Singleton1.instance;
		 Singleton1 instanceTwo = (Singleton1) instanceOne.clone();
		 System.out.println("hash code " + instanceOne);
		 System.out.println("hash code " + instanceTwo);

	}

}
