package palindrom;

import java.util.HashMap;
import java.util.Map;

public class RansomNoteChecker {
	
	public static boolean construct(String note,String magzine) {
		Map<Character,Integer>mchar=new HashMap<>();
		
		//count char
		for (char c : magzine.toCharArray()) {
			mchar.put(c, mchar.getOrDefault(c, 0)+1);
		}
		for (char c : note.toCharArray()) {
			if(!mchar.containsKey(c)||mchar.get(c)==0) {
				return false;
			}
				mchar.put(c, mchar.get(c)-1);
		}
		return true;
	}
	
	public static void main(String[] args) {
		String noteOne="ceba";
		String magzineOne="abcde";
		System.out.println(construct(noteOne,magzineOne));
		
		String noteTwo="deaa";
		String magzineTwo="abcde";
		System.out.println(construct(noteTwo, magzineTwo));
		
		String noteThree="aacc";
		String magzineThree="bbccaa";
		System.out.println(construct(noteThree, magzineThree));
		
	}

}
