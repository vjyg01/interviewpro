package palindrom;

import java.util.Scanner;
import java.util.stream.IntStream;

public class palindrom {

	public static void main(String[] args) {
		 Scanner sc=new Scanner(System.in);
	        String str=sc.nextLine();
	        System.out.println("enter the String " + str);
		System.out.println("ispalindrome  : " + isPalindrome(str));
	}

	public static boolean isPalindrome(String str) {
		//String temp = str.replaceAll("\\s", "").toLowerCase();

		return IntStream.range(0, str.length() / 2)
				.noneMatch(i -> str.charAt(i) != str.charAt(str.length() - i - 1));

	}

	/*
	 * public static boolean isPalindrome(String str) { int length = str.length();
	 * for (int i = 0; i < length / 2; i++) { if (str.charAt(i) != str.charAt(length
	 * - 1 - i)) { return false; } } return true; }
	 */
	
	
	/*
	 * public static void main(String[] args) { int arr[] = {1,2,5,4,3}; int
	 * n=arr.length; Scanner sc=new Scanner(System.in); int sum=sc.nextInt();
	 * pair(arr,n,sum);
	 */

	// }
	/*
	 * static void pair(int arr[],int n,int sum) {
	 * 
	 * for(int i=0;i<n;i++) { for(int j=i+1;j<n;j++) { if(arr[i]+arr[j]==sum) {
	 * System.out.println("pair prints " + arr[i] + ", " + arr[j]); } } }
	 * 
	 * Set<Integer> s = new HashSet<>(); for (int i = 0; i < n; i++) { int temp =
	 * sum - arr[i]; if (s.contains(temp)) { System.out.println("(" + temp + ", " +
	 * arr[i] + ")"); } s.add(arr[i]); } }
	 */
}
